<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * MovieList
 *
 * @ORM\Table(name="movie_list", indexes={@ORM\Index(name="IDX_B7AED9157E3C61F9", columns={"owner_id"})})
 * @ORM\Entity
 */
class MovieList
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var array|null
     *
     * @ORM\Column(name="movies", type="array", length=0, nullable=true)
     */
    private $movies;

    /**
     * @var int
     *
     * @ORM\Column(name="owner", type="integer", nullable=false)
     */
    private $owner;

    /**
     * @var array|null
     *
     * @ORM\Column(name="share_with", type="array", length=0, nullable=true)
     */
    private $shareWith;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     * })
     */
    private $owner2;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Movie", inversedBy="movieList")
     * @ORM\JoinTable(name="movie_list_movie",
     *   joinColumns={
     *     @ORM\JoinColumn(name="movie_list_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="movie_id", referencedColumnName="id")
     *   }
     * )
     */
    private $movie;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->movie = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMovies(): ?array
    {
        return $this->movies;
    }

    public function setMovies(?array $movies): self
    {
        $this->movies = $movies;

        return $this;
    }

    public function getOwner(): ?int
    {
        return $this->owner;
    }

    public function setOwner(int $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function getShareWith(): ?array
    {
        return $this->shareWith;
    }

    public function setShareWith(?array $shareWith): self
    {
        $this->shareWith = $shareWith;

        return $this;
    }

    public function getOwner2(): ?User
    {
        return $this->owner2;
    }

    public function setOwner2(?User $owner2): self
    {
        $this->owner2 = $owner2;

        return $this;
    }

    /**
     * @return Collection|Movie[]
     */
    public function getMovie(): Collection
    {
        return $this->movie;
    }

    public function addMovie(Movie $movie): self
    {
        if (!$this->movie->contains($movie)) {
            $this->movie[] = $movie;
        }

        return $this;
    }

    public function removeMovie(Movie $movie): self
    {
        if ($this->movie->contains($movie)) {
            $this->movie->removeElement($movie);
        }

        return $this;
    }

}
