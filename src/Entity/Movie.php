<?php

namespace App\Entity;

use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Movie
 *
 * @ORM\Table(name="movie", indexes={@ORM\Index(name="IDX_1D5EF26F899FB366", columns={"director_id"})})
 * @ORM\Entity
 */
class Movie
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="cover", type="string", length=255, nullable=false)
     */
    private $cover;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=0, nullable=false)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="production_year", type="date", nullable=false)
     */
    private $productionYear;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="release_date", type="date", nullable=false)
     */
    private $releaseDate;

    /**
     * @var string
     *
     * @ORM\Column(name="genre", type="string", length=255)
     */
    private $genre;

    /**
     * @var \MovieRelatedPerson
     *
     * @ORM\ManyToOne(targetEntity="MovieRelatedPerson")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="director_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $director;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="MovieList", mappedBy="movie")
     */
    private $movieList;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="MovieRelatedPerson", inversedBy="movie")
     * @ORM\JoinTable(name="movie_movie_related_person",
     *   joinColumns={
     *     @ORM\JoinColumn(name="movie_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="movie_related_person_id", referencedColumnName="id")
     *   }
     * )
     */
    private $movieRelatedPerson;

    /**
     * @var \MovieRelatedPerson
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $actor;

    /**
     * @ORM\Column(type="integer")
     */
    private $idOnApi;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->movieList = new \Doctrine\Common\Collections\ArrayCollection();
        $this->movieRelatedPerson = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getCover(): ?string
    {
        return $this->cover;
    }

    public function setCover(string $cover): self
    {
        $this->cover = $cover;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getProductionYear(): ?\DateTimeInterface
    {
        return $this->productionYear;
    }

    public function setProductionYear(\DateTimeInterface $productionYear): self
    {
        $this->productionYear = $productionYear;

        return $this;
    }

    public function getReleaseDate(): ?\DateTimeInterface
    {
        return $this->releaseDate;
    }

    public function setReleaseDate(\DateTimeInterface $releaseDate): self
    {
        $this->releaseDate = $releaseDate;

        return $this;
    }

    public function getGenre(): ?string
    {
        return $this->genre;
    }

    public function setGenre(string $genre): self
    {
        $this->genre = $genre;

        return $this;
    }

    public function getDirector(): ?MovieRelatedPerson
    {
        return $this->director;
    }

    public function setDirector(?MovieRelatedPerson $director): self
    {
        $this->director = $director;

        return $this;
    }

    /**
     * @return Collection|MovieList[]
     */
    public function getMovieList(): Collection
    {
        return $this->movieList;
    }

    public function addMovieList(MovieList $movieList): self
    {
        if (!$this->movieList->contains($movieList)) {
            $this->movieList[] = $movieList;
            $movieList->addMovie($this);
        }

        return $this;
    }

    public function removeMovieList(MovieList $movieList): self
    {
        if ($this->movieList->contains($movieList)) {
            $this->movieList->removeElement($movieList);
            $movieList->removeMovie($this);
        }

        return $this;
    }

    /**
     * @return Collection|MovieRelatedPerson[]
     */
    public function getMovieRelatedPerson(): Collection
    {
        return $this->movieRelatedPerson;
    }

    public function addMovieRelatedPerson(MovieRelatedPerson $movieRelatedPerson): self
    {
        if (!$this->movieRelatedPerson->contains($movieRelatedPerson)) {
            $this->movieRelatedPerson[] = $movieRelatedPerson;
        }

        return $this;
    }

    public function removeMovieRelatedPerson(MovieRelatedPerson $movieRelatedPerson): self
    {
        if ($this->movieRelatedPerson->contains($movieRelatedPerson)) {
            $this->movieRelatedPerson->removeElement($movieRelatedPerson);
        }

        return $this;
    }

    public function getActor(): ?string
    {
        return $this->actor;
    }

    public function setActor(string $actor): self
    {
        $this->actor = $actor;

        return $this;
    }

    public function getIdOnApi(): ?int
    {
        return $this->idOnApi;
    }

    public function setIdOnApi(int $idOnApi): self
    {
        $this->idOnApi = $idOnApi;

        return $this;
    }

}
