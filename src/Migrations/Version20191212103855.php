<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191212103855 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE movie_related_person (id INT AUTO_INCREMENT NOT NULL, first_name VARCHAR(255) NOT NULL, last_name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE movie (id INT AUTO_INCREMENT NOT NULL, director_id INT NOT NULL, title VARCHAR(255) NOT NULL, cover VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, production_year DATE NOT NULL, release_date DATE NOT NULL, genre VARCHAR(255) NOT NULL, INDEX IDX_1D5EF26F899FB366 (director_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE movie_movie_related_person (movie_id INT NOT NULL, movie_related_person_id INT NOT NULL, INDEX IDX_50496B158F93B6FC (movie_id), INDEX IDX_50496B15E187B54 (movie_related_person_id), PRIMARY KEY(movie_id, movie_related_person_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE movie_list (id INT AUTO_INCREMENT NOT NULL, owner_id INT NOT NULL, movies LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', owner INT NOT NULL, share_with LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', INDEX IDX_B7AED9157E3C61F9 (owner_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE movie_list_movie (movie_list_id INT NOT NULL, movie_id INT NOT NULL, INDEX IDX_2D3D16501D3854A5 (movie_list_id), INDEX IDX_2D3D16508F93B6FC (movie_id), PRIMARY KEY(movie_list_id, movie_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE movie ADD CONSTRAINT FK_1D5EF26F899FB366 FOREIGN KEY (director_id) REFERENCES movie_related_person (id)');
        $this->addSql('ALTER TABLE movie_movie_related_person ADD CONSTRAINT FK_50496B158F93B6FC FOREIGN KEY (movie_id) REFERENCES movie (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE movie_movie_related_person ADD CONSTRAINT FK_50496B15E187B54 FOREIGN KEY (movie_related_person_id) REFERENCES movie_related_person (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE movie_list ADD CONSTRAINT FK_B7AED9157E3C61F9 FOREIGN KEY (owner_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE movie_list_movie ADD CONSTRAINT FK_2D3D16501D3854A5 FOREIGN KEY (movie_list_id) REFERENCES movie_list (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE movie_list_movie ADD CONSTRAINT FK_2D3D16508F93B6FC FOREIGN KEY (movie_id) REFERENCES movie (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user CHANGE created_at confirm_mdp DATETIME NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE movie DROP FOREIGN KEY FK_1D5EF26F899FB366');
        $this->addSql('ALTER TABLE movie_movie_related_person DROP FOREIGN KEY FK_50496B15E187B54');
        $this->addSql('ALTER TABLE movie_movie_related_person DROP FOREIGN KEY FK_50496B158F93B6FC');
        $this->addSql('ALTER TABLE movie_list_movie DROP FOREIGN KEY FK_2D3D16508F93B6FC');
        $this->addSql('ALTER TABLE movie_list_movie DROP FOREIGN KEY FK_2D3D16501D3854A5');
        $this->addSql('DROP TABLE movie_related_person');
        $this->addSql('DROP TABLE movie');
        $this->addSql('DROP TABLE movie_movie_related_person');
        $this->addSql('DROP TABLE movie_list');
        $this->addSql('DROP TABLE movie_list_movie');
        $this->addSql('ALTER TABLE user CHANGE confirm_mdp created_at DATETIME NOT NULL');
    }
}
