<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191212134433 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE movie CHANGE director_id director_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE movie_movie_related_person DROP FOREIGN KEY FK_50496B158F93B6FC');
        $this->addSql('ALTER TABLE movie_movie_related_person DROP FOREIGN KEY FK_50496B15E187B54');
        $this->addSql('ALTER TABLE movie_movie_related_person ADD CONSTRAINT FK_50496B158F93B6FC FOREIGN KEY (movie_id) REFERENCES movie (id)');
        $this->addSql('ALTER TABLE movie_movie_related_person ADD CONSTRAINT FK_50496B15E187B54 FOREIGN KEY (movie_related_person_id) REFERENCES movie_related_person (id)');
        $this->addSql('ALTER TABLE movie_list CHANGE owner_id owner_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE movie_list_movie DROP FOREIGN KEY FK_2D3D16501D3854A5');
        $this->addSql('ALTER TABLE movie_list_movie DROP FOREIGN KEY FK_2D3D16508F93B6FC');
        $this->addSql('ALTER TABLE movie_list_movie ADD CONSTRAINT FK_2D3D16501D3854A5 FOREIGN KEY (movie_list_id) REFERENCES movie_list (id)');
        $this->addSql('ALTER TABLE movie_list_movie ADD CONSTRAINT FK_2D3D16508F93B6FC FOREIGN KEY (movie_id) REFERENCES movie (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE movie CHANGE director_id director_id INT NOT NULL');
        $this->addSql('ALTER TABLE movie_list CHANGE owner_id owner_id INT NOT NULL');
        $this->addSql('ALTER TABLE movie_list_movie DROP FOREIGN KEY FK_2D3D16501D3854A5');
        $this->addSql('ALTER TABLE movie_list_movie DROP FOREIGN KEY FK_2D3D16508F93B6FC');
        $this->addSql('ALTER TABLE movie_list_movie ADD CONSTRAINT FK_2D3D16501D3854A5 FOREIGN KEY (movie_list_id) REFERENCES movie_list (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE movie_list_movie ADD CONSTRAINT FK_2D3D16508F93B6FC FOREIGN KEY (movie_id) REFERENCES movie (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE movie_movie_related_person DROP FOREIGN KEY FK_50496B158F93B6FC');
        $this->addSql('ALTER TABLE movie_movie_related_person DROP FOREIGN KEY FK_50496B15E187B54');
        $this->addSql('ALTER TABLE movie_movie_related_person ADD CONSTRAINT FK_50496B158F93B6FC FOREIGN KEY (movie_id) REFERENCES movie (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE movie_movie_related_person ADD CONSTRAINT FK_50496B15E187B54 FOREIGN KEY (movie_related_person_id) REFERENCES movie_related_person (id) ON DELETE CASCADE');
    }
}
