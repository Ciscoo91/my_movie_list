<?php

// src/Controller/HelloController.php
namespace App\Controller;

use App\Entity\User;
use App\Entity\Movie;
use App\Form\UserType;
use App\Form\LoginType;
use App\Entity\MovieRelatedPerson;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class HelloController extends AbstractController
{
    /**
     * Page d'accueil
     *
     * @Route("/", name="home")
     */
    public function home()
    {
        return $this->render("index.html.twig");
    }
   
    /**
     * @Route("/register", name="register")
     */
    public function Register(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $newUser = new User();
        
        $form = $this->createForm(UserType::class, $newUser);
        $form->handleRequest($request);
        $repository = $this->getDoctrine()->getRepository(User::class);

        if($form->isSubmitted() && $form->isValid())
        {
            $newUser = $form->getData();
            
            if(!$newUser->getId())
            {
                $newUser->setCreationDate(new \Datetime());
            }
            $newUser->setModificationDate(new \Datetime());
            // $duplicateUser = $repository->findOneBy(["username" => $newUser->getUsername()]);
            
            $hashedPassword = $encoder->encodePassword($newUser, $newUser->getPassword());
            $newUser->setPassword($hashedPassword);
            $newUser->setStatus("simple_user");
            
            $userManager = $this->getDoctrine()->getManager();
            $userManager->persist($newUser);
            $userManager->flush();
            
            return $this->redirectToRoute('register', ["successMsg" => "Ce pseudo est déjà pris"]);
        }
        return $this->render('auth/register.html.twig', ['formRegister'=> $form->createView()]);
    }

     
    /**
     * 
     * @Route("/login", name="login")
     */
    public function login(Request $request)
    {
        return $this->render('auth/login.html.twig');
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logout()
    {
        
    }

    /**
     * @Route("/profile", name="profile")
     */
    public function profile()
    {
        return $this->render('profile/profile.html.twig', ["msg"=>"You have been connected successfully"]);
    }

    /**
     * @Route("/movies", name="movies")
     */
    public function movies(Request $request)
    {
        $api_key = $this->getParameter('the_movie_db.api_key');
        $movie = "Rambo";
        $client = HttpClient::create();

        // Query Movies
        $response = $client->request("GET", "https://api.themoviedb.org/3/search/movie?api_key=".$api_key."&query=".$movie);

        $statusCode = $response->getStatusCode();
        $contentType = $response->getHeaders()['content-type'][0];
        $content = $response->getContent();
        $content = $response->toArray();

        //Query Cast From First Movie In The Content List
        $movieId = $content["results"][0]["id"];
        $castResponse = $client->request("GET", "https://api.themoviedb.org/3/movie/".$movieId."/credits?api_key=".$api_key);
        $castContent = $castResponse->getContent();
        $castContent = $castResponse->toArray();
        $listCast = $castContent["cast"];
        // dump($listCast);

        
        return $this->render("movies/movies.html.twig", ["content" => $content, "firstcast" => $listCast]);
    }
    
    /**
     * @Route("/movie/register", name="movie_register")
     */
    public function registerMovie()
    {
        $movieManager = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository(MovieRelatedPerson::class);
        $client = HttpClient::create();
        $apiKey = $this->getParameter("the_movie_db.api_key");

        //Request Movie
        $requestMovies = "https://api.themoviedb.org/3/movie/top_rated?api_key=".$apiKey."&language=en-US&page=3";
        // $topFiftyMovies = [];

        for($i=0; $i<20; $i++)
        {
            $newMovie = new Movie();
            $newMovie->setTitle($topMovies[$i]["title"])
                    ->setCover($topMovies[$i]["poster_path"])
                    ->setDescription($topMovies[$i]["overview"])
                    ->setProductionYear(new \Datetime($topMovies[$i]["release_date"]))
                    ->setReleaseDate(new \Datetime($topMovies[$i]["release_date"]))
                    ->setGenre($topMovies[$i]["title"])
                    ->setIdOnApi($topMovies[$i]["id"]);

            $requestPeoples = "https://api.themoviedb.org/3/movie/".$topMovies[$i]["id"]."/credits?api_key=".$apiKey;
            // Request to API
            $result = $client->request("GET", $requestMovies);
            $resultPeople = $client->request("GET", $requestPeoples);
            // dump($resultPeople);

            $requestContent = $result->getContent();
            $requestContent = $result->toArray();
            $topMovies = $requestContent["results"];

            $requestContentPeople = $resultPeople->getContent();
            $requestContentPeople = $resultPeople->toArray();
            $crewMembers = $requestContentPeople["crew"];
            $castMembers = $requestContentPeople["cast"];

            //Querying Director of each movie
            $movieDirector;
            foreach($crewMembers as $row)
            {
                if($row["job"] === "Director")
                {
                    $movieDirector = $row;
                }
            }

            //Set the Director for Each Movie
            $newMovie->setDirector($movieDirector);
            
            // Querying Cast of Each Movie
            $listOfCast = []; //list to set to the movie
            foreach($castMembers as $member)
            {
                $fullName = explode($member["name"]);
                $firstName = $fullName[0];
                $lastName = $fullName[1];

                $cast = new MovieRelatedPerson();
                $cast->setFirstName($firstName);
                $cast->setLastName($lastName);
                $cast->setIdOnApi($member["id"]);

                $listOfCast[] = $cast->getIdOnApi();

                $movieManager->persist($cast);
            }
            // Set Actor list for each movie
            $newMovie->setActor($listOfCast);

            // Persist Movie and flush to the database
            $movieManager->persist($newMovie);

        }

        // $movieManager->flush();

        return $this->render('movies/movies.html.twig');
    }

}